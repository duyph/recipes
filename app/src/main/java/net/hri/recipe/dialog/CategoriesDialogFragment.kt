package net.hri.recipe.dialog

import android.app.Application
import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.viewModelScope
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.dialog_list_view.*
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.hri.recipe.R
import net.hri.recipe.adapter.FlexibleAdapter
import net.hri.recipe.binder.CategoryViewBinder
import net.hri.recipe.database.AppDatabase
import net.hri.recipe.model.Category
import net.hri.recipe.mutableLiveDataOf
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import vn.vtvlive.vtvpay.base.adapter.lists.base.SingleChoiceMode
import java.util.concurrent.atomic.AtomicBoolean

class CategoriesDialogFragment(private val type: Int = 0) : DialogFragment(),
    CategoryViewBinder.OnCategoryCallback {
    private var isPendingDenied = AtomicBoolean(false)
    private val mAdapter: FlexibleAdapter by inject()
    private val mViewModel: CategoriesViewModel by viewModel()
    private var mCallback: OnCategoryDialogListener? = null
    private val inAnimation by lazy {
        AnimationUtils.loadAnimation(
            requireContext(),
            R.anim.slide_up
        )
    }
    private val outAnimation by lazy {
        AnimationUtils.loadAnimation(
            requireContext(),
            R.anim.slide_down
        )
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(activity!!, theme) {
            override fun onBackPressed() {
                dismissWithAnim()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setStyle(STYLE_NO_TITLE, R.style.CustomDialogTheme)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_list_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter.register(CategoryViewBinder(this))
        mAdapter.setChoiceMode(SingleChoiceMode())
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = mAdapter
        view_root.startAnimation(inAnimation)
        conntainer.setOnClickListener { dismissWithAnim() }
        mViewModel.fetching()
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel.mCategoriesLive.observe(viewLifecycleOwner, Observer {
            mAdapter.submit(it)
            mAdapter.setSelected(indexOf(type, it), true)
        })
    }

    private fun dismissWithAnim() {
        if (!isPendingDenied.getAndSet(true)) {
            outAnimation?.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation?) = Unit
                override fun onAnimationRepeat(animation: Animation?) = Unit
                override fun onAnimationEnd(animation: Animation?) {
                    this@CategoriesDialogFragment.dismiss()
                    isPendingDenied.set(false)
                }
            })
            view_root.startAnimation(outAnimation)
        }
    }

    fun setOnCategorySelectedListener(callback: OnCategoryDialogListener) {
        mCallback = callback
    }

    override fun onCategoryClicked(holder: CategoryViewBinder.ViewHolder) {
        holder.mCategory?.let { mCallback?.onCategorySelected(it) }
        dismissWithAnim()
    }

    private fun indexOf(id: Int, items: List<Category>): Int {
        items.forEachIndexed { index, serviceItemNew ->
            if (serviceItemNew.id == id) {
                return index
            }
        }
        return 0
    }
}

interface OnCategoryDialogListener {
    fun onCategorySelected(category: Category)
}


class CategoriesViewModel(
    application: Application,
    private val dao: AppDatabase
) : AndroidViewModel(application) {
    val mCategoriesLive: MutableLiveData<List<Category>> = mutableLiveDataOf()
    private val mHandler = CoroutineExceptionHandler { _, throwable ->
        throwable.printStackTrace()
    }

    fun fetching() {
        viewModelScope.launch(mHandler) {
            val items = withContext(Dispatchers.IO) { dao.categories.getAll() }
            mCategoriesLive.value = items
        }
    }
}