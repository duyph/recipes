package net.hri.recipe.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.dialog_list_view.*
import net.hri.recipe.R
import net.hri.recipe.adapter.FlexibleAdapter
import net.hri.recipe.binder.IngredientViewBinder
import net.hri.recipe.model.Ingredient
import org.koin.android.ext.android.inject
import java.util.concurrent.atomic.AtomicBoolean

class IngredientsDialogFragment(private val items: List<Ingredient>?) : DialogFragment() {
    private var isPendingDenied = AtomicBoolean(false)
    private val mAdapter: FlexibleAdapter by inject()
    private val inAnimation by lazy {
        AnimationUtils.loadAnimation(
            requireContext(),
            R.anim.slide_up
        )
    }
    private val outAnimation by lazy {
        AnimationUtils.loadAnimation(
            requireContext(),
            R.anim.slide_down
        )
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return object : Dialog(activity!!, theme) {
            override fun onBackPressed() {
                dismissWithAnim()
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setStyle(STYLE_NO_TITLE, R.style.CustomDialogTheme)
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.dialog_list_view, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter.register(IngredientViewBinder())

        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = mAdapter
        view_root.startAnimation(inAnimation)
        mAdapter.submit(items?: mutableListOf())
        conntainer.setOnClickListener { dismissWithAnim() }
    }

    private fun dismissWithAnim() {
        if (!isPendingDenied.getAndSet(true)) {
            outAnimation?.setAnimationListener(object : Animation.AnimationListener {
                override fun onAnimationStart(animation: Animation?) = Unit
                override fun onAnimationRepeat(animation: Animation?) = Unit
                override fun onAnimationEnd(animation: Animation?) {
                    this@IngredientsDialogFragment.dismiss()
                    isPendingDenied.set(false)
                }
            })
            view_root.startAnimation(outAnimation)
        }
    }
}