package net.hri.recipe.model

data class Event<T>(
    val tag: String = "",
    val data: T
)