package net.hri.recipe.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "recipe_entity")
data class Recipe(
    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @ColumnInfo(name = "name") var name: String = "",
    @ColumnInfo(name = "desc") var desc: String = "",
    @ColumnInfo(name = "ingredients") var ingredients: List<Ingredient> = mutableListOf(),
    @ColumnInfo(name = "thumbnail") var thumbnail: String = "",
    @ColumnInfo(name = "type") var type: Int = 0,
    @ColumnInfo(name = "steps") var steps: String = ""

) : IFlexibleItem, Parcelable {
    override fun areItemsTheSame(iFlexibleItem: IFlexibleItem): Boolean =
        iFlexibleItem is Recipe && iFlexibleItem.id == id

    override fun areContentsTheSame(iFlexibleItem: IFlexibleItem): Boolean =
        iFlexibleItem is Recipe && iFlexibleItem.id == id
                && iFlexibleItem.name == name
                && iFlexibleItem.desc == desc
                && iFlexibleItem.ingredients.containsAll(ingredients)
                && iFlexibleItem.thumbnail == thumbnail
                && iFlexibleItem.type == type
                && iFlexibleItem.steps == steps

    override fun equals(other: Any?): Boolean {
        return other is IFlexibleItem && areContentsTheSame(other)
    }

    override fun hashCode(): Int {
        return super.hashCode()
    }

    fun getMaxIngredients(): List<Ingredient> {
        if (ingredients.size > 5) return ingredients.subList(0, 4)
        return ingredients
    }

    data class Content(val content: String) : IFlexibleItem {
        override fun areItemsTheSame(iFlexibleItem: IFlexibleItem): Boolean =
            iFlexibleItem is Content && iFlexibleItem.content == content

        override fun areContentsTheSame(iFlexibleItem: IFlexibleItem): Boolean =
            areItemsTheSame(iFlexibleItem)
    }
}