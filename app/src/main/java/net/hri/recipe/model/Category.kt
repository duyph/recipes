package net.hri.recipe.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "category_entity")
data class Category(
    @PrimaryKey var id: Int = 0,
    var name: String = "",
    @Ignore var isSelected: Boolean = false
) : IFlexibleItem {
    override fun areItemsTheSame(iFlexibleItem: IFlexibleItem): Boolean =
        iFlexibleItem is Category && iFlexibleItem.id == id

    override fun areContentsTheSame(iFlexibleItem: IFlexibleItem): Boolean =
        iFlexibleItem is Category
                && iFlexibleItem.id == id
                && iFlexibleItem.name == name
                && iFlexibleItem.isSelected == isSelected
}