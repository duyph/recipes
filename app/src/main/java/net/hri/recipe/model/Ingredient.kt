package net.hri.recipe.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Ingredient(
    var amount: Float = 0f,
    var unit: String = "",
    var name: String = "",
    var icon: Int = 0
) : IFlexibleItem, Parcelable {
    override fun areItemsTheSame(iFlexibleItem: IFlexibleItem): Boolean =
        iFlexibleItem is Ingredient && iFlexibleItem.name == name

    override fun areContentsTheSame(iFlexibleItem: IFlexibleItem): Boolean =
        iFlexibleItem is Ingredient
                && iFlexibleItem.name == name
                && iFlexibleItem.unit == unit
                && iFlexibleItem.amount == amount
                && iFlexibleItem.icon == icon

    fun getAmountAndUnit(): String = "$amount $unit"
}