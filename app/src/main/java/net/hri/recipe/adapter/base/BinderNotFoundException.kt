package net.hri.recipe.adapter.base


internal class BinderNotFoundException(clazz: Class<*>) : RuntimeException(
  "Have you registered the ${clazz.name} type and its binder to the adapter or types?"
)
