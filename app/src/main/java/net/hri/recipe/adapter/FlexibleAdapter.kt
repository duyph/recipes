package net.hri.recipe.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AdapterListUpdateCallback
import androidx.recyclerview.widget.AsyncDifferConfig
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.RecyclerView
import net.hri.recipe.adapter.base.*
import net.hri.recipe.model.IFlexibleItem
import vn.vtvlive.vtvpay.base.adapter.lists.base.ChoiceMode

class FlexibleAdapter(
        override val types: Types = MutableTypes(),
        private var mChoiceMode: ChoiceMode? = null
) : IAdapter<List<IFlexibleItem>>,
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val differ: AsyncListDiffer<IFlexibleItem>

    init {
        val diffUtil = ItemDiffUtil()
        val callback = AdapterListUpdateCallback(this)
        val config = AsyncDifferConfig.Builder(diffUtil).build()
        differ = AsyncListDiffer(callback, config)
    }

    override fun onCreateViewHolder(parent: ViewGroup, indexViewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binder = types.getType<IFlexibleItem>(indexViewType).binder
        return binder.onCreateViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        onBindViewHolder(holder, position, mutableListOf())
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        val item = differ.currentList[position]
        getOutBinderByViewHolder(holder).onBindViewHolder(holder, item, payloads)
    }

    override fun getItemViewType(position: Int): Int {
        return indexInTypesOf(position, differ.currentList[position])
    }

    /**
     * Called when a view created by this adapter has been recycled, and passes the event to its
     * associated binder.
     *
     * @param holder The ViewHolder for the view being recycled
     * @see RecyclerView.Adapter.onViewRecycled
     * @see ItemViewBinder.onViewRecycled
     */
    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        getOutBinderByViewHolder(holder).onViewRecycled(holder)
    }

    override fun submit(data: List<IFlexibleItem>) {
        differ.submitList(data)
    }

    /**
     * Called by the RecyclerView if a ViewHolder created by this Adapter cannot be recycled
     * due to its transient state, and passes the event to its associated item view binder.
     *
     * @param holder The ViewHolder containing the View that could not be recycled due to its
     * transient state.
     * @return True if the View should be recycled, false otherwise. Note that if this method
     * returns `true`, RecyclerView *will ignore* the transient state of
     * the View and recycle it regardless. If this method returns `false`,
     * RecyclerView will check the View's transient state again before giving a final decision.
     * Default implementation returns false.
     * @see RecyclerView.Adapter.onFailedToRecycleView
     * @see ItemViewBinder.onFailedToRecycleView
     */
    override fun onFailedToRecycleView(holder: RecyclerView.ViewHolder): Boolean {
        return getOutBinderByViewHolder(holder).onFailedToRecycleView(holder)
    }

    /**
     * Called when a view created by this adapter has been attached to a window, and passes the
     * event to its associated item view binder.
     *
     * @param holder Holder of the view being attached
     * @see RecyclerView.Adapter.onViewAttachedToWindow
     * @see ItemViewBinder.onViewAttachedToWindow
     */
    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        getOutBinderByViewHolder(holder).onViewAttachedToWindow(holder)
    }

    /**
     * Called when a view created by this adapter has been detached from its window, and passes
     * the event to its associated item view binder.
     *
     * @param holder Holder of the view being detached
     * @see RecyclerView.Adapter.onViewDetachedFromWindow
     * @see ItemViewBinder.onViewDetachedFromWindow
     */
    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        getOutBinderByViewHolder(holder).onViewDetachedFromWindow(holder)
    }

    /**
     * Called to return the stable ID for the item, and passes the event to its associated binder.
     *
     * @param position Adapter position to query
     * @return the stable ID of the item at position
     * @see ItemViewBinder.getItemId
     * @see RecyclerView.Adapter.setHasStableIds
     */
    override fun getItemId(position: Int): Long {
        val item = differ.currentList[position]
        val itemViewType = getItemViewType(position)
        return types.getType<IFlexibleItem>(itemViewType).binder.getItemId(item)
    }

    override fun getItemCount(): Int = differ.currentList.size

    inline fun <reified T : IFlexibleItem> register(binder: ItemViewBinder<T, *>) {
        register(T::class, binder)
    }

    fun setChoiceMode(mode: ChoiceMode) {
        mChoiceMode = mode
    }

    fun setSelected(position: Int, isSelected: Boolean) {
        mChoiceMode?.let {
            if (it.isSingleChoice()) {
                val selected = it.getCheckedPosition()
                if (selected >= 0) {
                    val unSelectedData = getItem(selected)
                    val viewType = getItemViewType(selected)
                    val types = types.getType<IFlexibleItem>(viewType)
                    types.binder.notifySelectedState(unSelectedData, !isSelected)
                    notifyItemChanged(selected)
                }
            }
            val selectedData = getItem(position)
            val viewType = getItemViewType(position)
            val types = types.getType<IFlexibleItem>(viewType)
            types.binder.notifySelectedState(selectedData, isSelected)
            notifyItemChanged(position)
            it.setChecked(position, isSelected)
        }
    }

    fun getItem(position: Int): IFlexibleItem = differ.currentList[position]
}