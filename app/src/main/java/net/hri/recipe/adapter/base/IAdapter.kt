package net.hri.recipe.adapter.base

import android.util.Log
import androidx.annotation.CheckResult
import androidx.recyclerview.widget.RecyclerView
import kotlin.reflect.KClass

interface IAdapter<Data> {
    val types: Types

    /**
     * Registers a type class and its item view binder. If you have registered the class,
     * it will override the original binder(s). Note that the method is non-thread-safe
     * so that you should not use it in concurrent operation.
     *
     * Note that the method should not be called after
     * [RecyclerView.setAdapter], or you have to call the setAdapter
     * again.
     *
     * @param clazz the class of a item
     * @param binder the item view binder
     * @param T the item data type
     * */
    fun <T> register(clazz: Class<T>, binder: ItemViewBinder<T, *>) {
        unregisterAllTypesIfNeeded(clazz)
        register(Type(clazz, binder, DefaultLinker()))
    }

    /**
     * Registers a type class to multiple item view binders. If you have registered the
     * class, it will override the original binder(s). Note that the method is non-thread-safe
     * so that you should not use it in concurrent operation.
     *
     * Note that the method should not be called after
     * [RecyclerView.setAdapter], or you have to call the setAdapter again.
     *
     * @param clazz the class of a item
     * @param <T> the item data type
     * @return [OneToManyFlow] for setting the binders
     * @see [register]
     */
    @CheckResult
    fun <T> register(clazz: Class<T>): OneToManyFlow<T> {
        unregisterAllTypesIfNeeded(clazz)
        return OneToManyBuilder(this, clazz)
    }

    @CheckResult
    fun <T : Any> register(clazz: KClass<T>): OneToManyFlow<T> {
        return register(clazz.java)
    }

    fun <T> register(type: Type<T>) {
        types.register(type)
        type.binder._adapter = this
    }

    fun <T : Any> register(clazz: KClass<T>, binder: ItemViewBinder<T, *>) {
        register(clazz.java, binder)
    }

    /**
     * Registers all of the contents in the specified [Types]. If you have registered a
     * class, it will override the original binder(s). Note that the method is non-thread-safe
     * so that you should not use it in concurrent operation.
     *
     * Note that the method should not be called after
     * [RecyclerView.setAdapter], or you have to call the setAdapter
     * again.
     *
     * @param types a [Types] containing contents to be added to this adapter inner [Types]
     * @see [register]
     * @see [register]
     */
    fun registerAll(types: Types) {
        val size = types.size
        for (i in 0 until size) {
            val type = types.getType<Any>(i)
            unregisterAllTypesIfNeeded(type.clazz)
            register(type)
        }
    }

    fun getOutBinderByViewHolder(holder: RecyclerView.ViewHolder): ItemViewBinder<Any, RecyclerView.ViewHolder> {
        @Suppress("UNCHECKED_CAST")
        return types.getType<Any>(holder.itemViewType).binder as ItemViewBinder<Any, RecyclerView.ViewHolder>
    }

    private fun unregisterAllTypesIfNeeded(clazz: Class<*>) {
        if (types.unregister(clazz)) {
            Log.w(TAG, "The type ${clazz.simpleName} you originally registered is now overwritten.")
        }
    }

    @Throws(BinderNotFoundException::class)
    fun indexInTypesOf(position: Int, item: Any?): Int {
        if (item == null) return 0
        val index = types.firstIndexOf(item.javaClass)
        if (index != -1) {
            val linker = types.getType<Any>(index).linker
            return index + linker.index(position, item)
        }
        throw BinderNotFoundException(item.javaClass)
    }

    fun submit(data: Data)


    companion object {
        private val TAG = IAdapter::class.java.simpleName
    }
}