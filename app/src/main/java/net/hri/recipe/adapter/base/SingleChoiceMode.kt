package vn.vtvlive.vtvpay.base.adapter.lists.base

import android.os.Bundle

class SingleChoiceMode : ChoiceMode {
    private var checkedPosition = -1
    override fun isSingleChoice(): Boolean = true

    override fun getCheckedPosition(): Int = checkedPosition

    override fun setChecked(position: Int, isChecked: Boolean) {
        if (isChecked) {
            checkedPosition = position;
        } else if (isChecked(position)) {
            checkedPosition = -1;
        }
    }

    override fun isChecked(position: Int): Boolean = checkedPosition == position

    override fun onSaveInstanceState(state: Bundle) {
        state.putInt(CHECKED_STATE, checkedPosition)
    }

    override fun onRestoreInstanceState(state: Bundle) {
        checkedPosition = state.getInt(CHECKED_STATE, -1)
    }

    companion object {
        const val CHECKED_STATE = "checked-state"
    }
}