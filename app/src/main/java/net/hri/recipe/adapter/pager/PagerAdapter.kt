package net.hri.recipe.adapter.pager

import android.util.SparseArray
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import net.example.pager.TabItem

class PagerAdapter(
    fm: FragmentManager,
    private val callback: OnPagerAdapterCallback
) : FragmentStatePagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    private var mItems: List<TabItem> = mutableListOf()
    private val mSparseArray: SparseArray<Fragment> = SparseArray()
    private var isShowTitle = true
    override fun getItem(position: Int): Fragment {
        return callback.getItem(mItems[position], position)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        val fragment = super.instantiateItem(container, position)
        mSparseArray.put(position, fragment as Fragment)
        return fragment
    }

    override fun destroyItem(
        container: ViewGroup,
        position: Int,
        `object`: Any
    ) {
        super.destroyItem(container, position, `object`)
        mSparseArray.remove(position)
    }

    override fun getCount(): Int {
        return mItems.size
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return if (isShowTitle) mItems[position].name else super.getPageTitle(position)
    }

    var items: List<TabItem>
        get() = mItems
        set(data) {
            this.mItems = data
            notifyDataSetChanged()
        }

    fun findFragmentByPosition(position: Int): Fragment {
        return mSparseArray[position]
    }
}