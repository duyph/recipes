package vn.vtvlive.vtvpay.base.adapter.lists.base

import android.os.Bundle

interface ChoiceMode {
    fun isSingleChoice(): Boolean
    fun getCheckedPosition(): Int
    fun setChecked(position: Int, isChecked: Boolean)
    fun isChecked(position: Int): Boolean
    fun onSaveInstanceState(state: Bundle)
    fun onRestoreInstanceState(state: Bundle)
}