package net.hri.recipe.adapter.pager

import androidx.fragment.app.Fragment
import net.example.pager.TabItem

interface OnPagerAdapterCallback {
    fun getItem(item: TabItem?, position: Int): Fragment
}