package net.hri.recipe.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedList
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import net.hri.recipe.adapter.base.*
import net.hri.recipe.model.IFlexibleItem

class PageListAdapter(
    override val types: Types = MutableTypes(),
    diffUtil: ItemDiffUtil = ItemDiffUtil()
) : PagedListAdapter<IFlexibleItem, RecyclerView.ViewHolder>(diffUtil),
    IAdapter<PagedList<out IFlexibleItem>> {

    override fun onCreateViewHolder(parent: ViewGroup, indexViewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binder = types.getType<IFlexibleItem>(indexViewType).binder
        return binder.onCreateViewHolder(inflater, parent)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        onBindViewHolder(holder, position, mutableListOf())
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int, payloads: MutableList<Any>) {
        getItem(position)?.also {
            getOutBinderByViewHolder(holder).onBindViewHolder(holder, it, payloads)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return indexInTypesOf(position, getItem(position))
    }

    /**
     * Called when a view created by this adapter has been recycled, and passes the event to its
     * associated binder.
     *
     * @param holder The ViewHolder for the view being recycled
     * @see RecyclerView.Adapter.onViewRecycled
     * @see ItemViewBinder.onViewRecycled
     */
    override fun onViewRecycled(holder: RecyclerView.ViewHolder) {
        getOutBinderByViewHolder(holder).onViewRecycled(holder)
    }

    @Suppress("UNCHECKED_CAST")
    override fun submit(data: PagedList<out IFlexibleItem>) {
        submitList(data as PagedList<IFlexibleItem>)
    }

    /**
     * Called by the RecyclerView if a ViewHolder created by this Adapter cannot be recycled
     * due to its transient state, and passes the event to its associated item view binder.
     *
     * @param holder The ViewHolder containing the View that could not be recycled due to its
     * transient state.
     * @return True if the View should be recycled, false otherwise. Note that if this method
     * returns `true`, RecyclerView *will ignore* the transient state of
     * the View and recycle it regardless. If this method returns `false`,
     * RecyclerView will check the View's transient state again before giving a final decision.
     * Default implementation returns false.
     * @see RecyclerView.Adapter.onFailedToRecycleView
     * @see ItemViewBinder.onFailedToRecycleView
     */
    override fun onFailedToRecycleView(holder: RecyclerView.ViewHolder): Boolean {
        return getOutBinderByViewHolder(holder).onFailedToRecycleView(holder)
    }

    /**
     * Called when a view created by this adapter has been attached to a window, and passes the
     * event to its associated item view binder.
     *
     * @param holder Holder of the view being attached
     * @see RecyclerView.Adapter.onViewAttachedToWindow
     * @see ItemViewBinder.onViewAttachedToWindow
     */
    override fun onViewAttachedToWindow(holder: RecyclerView.ViewHolder) {
        getOutBinderByViewHolder(holder).onViewAttachedToWindow(holder)
    }

    /**
     * Called when a view created by this adapter has been detached from its window, and passes
     * the event to its associated item view binder.
     *
     * @param holder Holder of the view being detached
     * @see RecyclerView.Adapter.onViewDetachedFromWindow
     * @see ItemViewBinder.onViewDetachedFromWindow
     */
    override fun onViewDetachedFromWindow(holder: RecyclerView.ViewHolder) {
        getOutBinderByViewHolder(holder).onViewDetachedFromWindow(holder)
    }

    /**
     * Called to return the stable ID for the item, and passes the event to its associated binder.
     *
     * @param position Adapter position to query
     * @return the stable ID of the item at position
     * @see ItemViewBinder.getItemId
     * @see RecyclerView.Adapter.setHasStableIds
     */
    override fun getItemId(position: Int): Long {
        val item = getItem(position)
        val itemViewType = getItemViewType(position)
        return if (item == null) RecyclerView.NO_ID
        else types.getType<IFlexibleItem>(itemViewType).binder.getItemId(item)
    }

    override fun getItemCount(): Int = currentList?.size ?: 0

    inline fun <reified T : IFlexibleItem> register(binder: ItemViewBinder<T, *>) {
        register(T::class.java, binder)
    }
}