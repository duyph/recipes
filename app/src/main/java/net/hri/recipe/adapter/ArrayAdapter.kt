package net.hri.recipe.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import kotlinx.android.synthetic.main.spinner_item_view.view.*
import net.hri.recipe.R
import net.hri.recipe.model.Category

class ArrayAdapter @JvmOverloads constructor(
    context: Context,
    items: List<Category> = mutableListOf(),
    resourceId: Int = 0
) : ArrayAdapter<Category>(context, resourceId, items) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getRootView(position, parent) ?: super.getView(position, convertView, parent)
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        return getRootView(position, parent) ?: super.getDropDownView(position, convertView, parent)
    }

    private fun getRootView(position: Int, parent: ViewGroup): View? {
        val inflater = context.getSystemService(LayoutInflater::class.java)
        val view = inflater?.inflate(R.layout.spinner_item_view, parent, false)
        val item = getItem(position)
        view?.rootView?.tv_title?.text = item?.name
        return view
    }

}