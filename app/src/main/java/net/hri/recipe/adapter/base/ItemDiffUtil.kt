package net.hri.recipe.adapter.base

import androidx.recyclerview.widget.DiffUtil
import net.hri.recipe.model.IFlexibleItem


class ItemDiffUtil : DiffUtil.ItemCallback<IFlexibleItem>() {

    override fun areItemsTheSame(oldItem: IFlexibleItem, newItem: IFlexibleItem): Boolean {
        return oldItem.areItemsTheSame(newItem)
    }

    override fun areContentsTheSame(oldItem: IFlexibleItem, newItem: IFlexibleItem): Boolean {
        return oldItem.areContentsTheSame(newItem)
    }
}
