package net.hri.recipe.ui

import android.app.Application
import net.hri.recipe.module.mAppModule
import net.hri.recipe.module.mViewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidFileProperties
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class Application : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidLogger()
            androidContext(this@Application)
            androidFileProperties()
            modules(mAppModule, mViewModelModule)
        }
    }
}