package net.hri.recipe.ui.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_main_view.*
import net.example.pager.TabItem
import net.hri.recipe.R
import net.hri.recipe.adapter.pager.OnPagerAdapterCallback
import net.hri.recipe.adapter.pager.PagerAdapter
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainFragment : Fragment(R.layout.fragment_main_view), OnPagerAdapterCallback {
    private val mViewModel: MainViewModel by viewModel()
    private val mViewPagerAdapter: PagerAdapter by lazy { PagerAdapter(childFragmentManager, this) }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mViewModel.geCategories()
        tabs.setupWithViewPager(view_pager)
        view_pager.adapter = mViewPagerAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel.mCategoriesLive.observe(viewLifecycleOwner, Observer {
            mViewPagerAdapter.items = it
        })
    }

    override fun getItem(item: TabItem?, position: Int): Fragment = RecipeFragment(item?.id ?: 0)
}