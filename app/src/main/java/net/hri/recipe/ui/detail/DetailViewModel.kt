package net.hri.recipe.ui.detail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.hri.recipe.R
import net.hri.recipe.database.AppDatabase
import net.hri.recipe.model.IFlexibleItem
import net.hri.recipe.model.Recipe
import net.hri.recipe.mutableLiveDataOf

class DetailViewModel(
    application: Application,
    private val dao: AppDatabase
) : AndroidViewModel(application) {
    val mDetailRecipeLive: MutableLiveData<List<IFlexibleItem>> = mutableLiveDataOf()
    val mErrorLive: MutableLiveData<Int> = mutableLiveDataOf()
    val mHeaderRecipeLive: MutableLiveData<Recipe> = mutableLiveDataOf()
    val mCategoryRecipeLive: MutableLiveData<String> = mutableLiveDataOf()
    var mRecipe: Recipe = Recipe()
    private val mHandler = CoroutineExceptionHandler { _, throwable ->
        throwable.printStackTrace()
    }

    fun handleRecipeDetail(id: Int) {
        viewModelScope.launch(mHandler) {
            val items: List<IFlexibleItem> = withContext(Dispatchers.IO) {
                val recipe = dao.recipes.getRecipe(id)
                recipe?.apply {
                    mRecipe = this
                    mHeaderRecipeLive.postValue(recipe)
                    handleCategoryOfRecipe(type)
                    val items: MutableList<IFlexibleItem> = mutableListOf()
                    items.add(this)
                    items.add(Recipe.Content(this.steps))
                    return@withContext items
                }
                return@withContext mutableListOf<IFlexibleItem>()
            }
            mDetailRecipeLive.value = items
        }
    }


    fun save() {
        when {
            mRecipe.name.isEmpty() -> mErrorLive.value = R.string.recipe_name_not_be_empty
            mRecipe.ingredients.isEmpty() -> mErrorLive.value = R.string.ingredient_not_be_null
            mRecipe.type == 0 -> mErrorLive.value = R.string.recipe_type_not_be_null
            else -> viewModelScope.launch(mHandler) {
                withContext(Dispatchers.IO) {
                    dao.recipes.save(mRecipe)
                }
            }
        }
    }

    private fun handleCategoryOfRecipe(type: Int) {
        val category = dao.categories.getCategory(type)
        category?.also { mCategoryRecipeLive.postValue(it.name) }
    }
}