package net.hri.recipe.ui.home

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_recipe_view.*
import net.hri.recipe.R
import net.hri.recipe.adapter.FlexibleAdapter
import net.hri.recipe.binder.RecipeViewBinder
import org.koin.android.ext.android.inject

class RecipeFragment(private val type: Int) : Fragment(R.layout.fragment_recipe_view),
    RecipeViewBinder.OnRecipeListener {
    private val mAdapter: FlexibleAdapter by inject()
    private val mViewModel: MainViewModel by inject()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mAdapter.register(RecipeViewBinder(this))

        mViewModel.getRecipesByCategory(type)
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = mAdapter
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel.mRecipesLive.observe(viewLifecycleOwner, Observer { mAdapter.submit(it) })
    }

    override fun onRecipeClicked(holder: RecipeViewBinder.ViewHolder, view: View) {
        val directions = MainFragmentDirections.actMainDetail()
        directions.recipeId = holder.mRecipe?.id ?: 0
        directions.typeId = holder.mRecipe?.type ?: 0
        findNavController().navigate(directions)
    }
}