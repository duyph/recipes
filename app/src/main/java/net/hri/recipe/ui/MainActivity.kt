package net.hri.recipe.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import net.hri.recipe.R

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
