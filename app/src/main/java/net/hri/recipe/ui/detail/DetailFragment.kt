package net.hri.recipe.ui.detail

import android.os.Bundle
import android.text.InputType
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_detail_recipe_view.*
import net.hri.recipe.R
import net.hri.recipe.adapter.FlexibleAdapter
import net.hri.recipe.binder.IngredientsViewBinder
import net.hri.recipe.binder.RecipeStepViewBinder
import net.hri.recipe.dialog.CategoriesDialogFragment
import net.hri.recipe.dialog.OnCategoryDialogListener
import net.hri.recipe.hideKeyboard
import net.hri.recipe.model.Category
import net.hri.recipe.model.Event
import net.hri.recipe.toast
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel


class DetailFragment : Fragment(R.layout.fragment_detail_recipe_view), OnCategoryDialogListener {
    private val mAdapter: FlexibleAdapter by inject()
    private val mViewModel: DetailViewModel by viewModel()
    private val mArguments: DetailFragmentArgs by navArgs()
    private var isEditable: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        activity?.window?.setFlags(
            WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN,
            WindowManager.LayoutParams.FLAG_LAYOUT_IN_SCREEN
        )
        super.onCreate(savedInstanceState)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mAdapter.register(IngredientsViewBinder(childFragmentManager))
        mAdapter.register(RecipeStepViewBinder())
        handleEditableUI(isEditable)
        recycler_view.layoutManager = LinearLayoutManager(context)
        recycler_view.adapter = mAdapter
        mViewModel.handleRecipeDetail(mArguments.recipeId)
        mViewModel.mRecipe.type = mArguments.typeId
        tv_recipe_name.addTextChangedListener(onTextChanged = { s, _, _, _ ->
            mViewModel.mRecipe.name = s.toString()
        })
        tv_desc.addTextChangedListener(onTextChanged = { s, _, _, _ ->
            mViewModel.mRecipe.desc = s.toString()
        })
        tv_category.setOnClickListener {
            if (isEditable) {
                val dialog = CategoriesDialogFragment(mArguments.typeId)
                dialog.setOnCategorySelectedListener(this)
                dialog.showNow(childFragmentManager, null)
            }
        }


        fab.setOnClickListener {
            if (isEditable) mViewModel.save()
            isEditable = !isEditable
            handleEditableUI(isEditable, true)
            fab.setImageResource(
                if (isEditable) R.drawable.ic_save_black_24dp
                else R.drawable.ic_edit_black_24dp
            )
        }
    }

    override fun onResume() {
        super.onResume()
        EventBus.getDefault().register(this)
    }

    override fun onPause() {
        super.onPause()
        EventBus.getDefault().unregister(this)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel.mHeaderRecipeLive.observe(viewLifecycleOwner, Observer {
            tv_app_name.text = it.name
            tv_recipe_name.setText(it.name)
            tv_desc.setText(it.desc)
            Picasso.get().load(it.thumbnail)
                .placeholder(R.drawable.bg_default)
                .error(R.drawable.bg_default)
                .into(iv_thumbnail)
        })
        mViewModel.mDetailRecipeLive.observe(viewLifecycleOwner, Observer { mAdapter.submit(it) })
        mViewModel.mErrorLive.observe(viewLifecycleOwner, Observer { getString(it).toast(context) })
        mViewModel.mCategoryRecipeLive.observe(
            viewLifecycleOwner,
            Observer { tv_category.text = it })
    }

    @Suppress("unused")
    @Subscribe(threadMode = ThreadMode.MAIN)
    fun onRecipeStepChangedListener(event: String) {
        Log.e("kjskdjskdjksd", event)
        mViewModel.mRecipe.steps = event
    }

    /// https://github.com/ravindu1024/android-keyboardlistener/blob/master/keyboard-listener/src/main/java/com/rw/keyboardlistener/KeyboardUtils.java
    private fun handleEditableUI(isEditable: Boolean, autoSave: Boolean = false) {
        val bgEditableTransparent = if (isEditable) R.drawable.bg_editable_view
        else android.R.color.transparent
        val bgEditableBorder = if (isEditable) R.drawable.bg_editable_view
        else R.drawable.bg_white_border_transparent_view
        tv_recipe_name.setBackgroundResource(bgEditableTransparent)
        tv_desc.setBackgroundResource(bgEditableTransparent)
        tv_category.setBackgroundResource(bgEditableBorder)
        focusable(tv_recipe_name, isEditable)
        focusable(tv_desc, isEditable)
        EventBus.getDefault().post(Event(data = isEditable))
        handleEditableFeature(isEditable)
    }


    private fun handleEditableFeature(isEditable: Boolean) {
        val inputType = if (isEditable) InputType.TYPE_CLASS_TEXT else InputType.TYPE_CLASS_TEXT or
                InputType.TYPE_TEXT_FLAG_MULTI_LINE
        tv_recipe_name.inputType = inputType
        tv_desc.inputType = inputType
        tv_desc.maxLines = 2

        if (!isEditable) tv_recipe_name.hideKeyboard()
    }

    private fun focusable(edt: EditText, isEditable: Boolean) {
        edt.isFocusable = isEditable
        edt.isFocusableInTouchMode = isEditable
    }

    override fun onCategorySelected(category: Category) {
        tv_category.text = category.name
        mViewModel.mRecipe.type = category.id
    }
}