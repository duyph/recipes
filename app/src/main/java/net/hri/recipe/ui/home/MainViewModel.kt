package net.hri.recipe.ui.home

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.annimon.stream.Stream
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import net.example.pager.TabItem
import net.hri.recipe.database.AppDatabase
import net.hri.recipe.model.Recipe
import net.hri.recipe.mutableLiveDataOf

class MainViewModel(
    application: Application,
    private val repository: IRecipeRepository,
    private val dao: AppDatabase
) : AndroidViewModel(application) {
    val mCategoriesLive: MutableLiveData<List<TabItem>> = mutableLiveDataOf()
    val mRecipesLive: MutableLiveData<List<Recipe>> = mutableLiveDataOf()
    private val mHandler = CoroutineExceptionHandler { _, throwable ->
        throwable.printStackTrace()
    }

    fun geCategories() {
        viewModelScope.launch(mHandler) {
            val items = withContext(Dispatchers.IO) {
                var categories = dao.categories.getAll()
                if (categories.isEmpty()) {
                    categories = repository.getRecipeTypes()
                    dao.categories.save(*categories.toTypedArray())
                }
                Stream.of(categories).map { TabItem(it.id, it.name) }.toList()
            }

            mCategoriesLive.value = items
        }
    }

    fun getRecipesByCategory(type: Int) {
        viewModelScope.launch(mHandler) {
            val items = withContext(Dispatchers.IO) {
                return@withContext if (dao.recipes.getCount() == 0) {
                    val items = repository.getRecipes()
                    dao.recipes.save(*items.toTypedArray())
                    Stream.of(items).filter { it.type == type }.toList()
                } else dao.recipes.getRecipesByType(type)
            }
            mRecipesLive.value = items
        }
    }
}