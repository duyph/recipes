package net.hri.recipe.ui.home

import android.content.res.AssetManager
import android.content.res.Resources
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.dataformat.xml.XmlMapper
import net.hri.recipe.model.Category
import net.hri.recipe.model.Recipe
import java.io.BufferedReader
import java.io.IOException

interface IRecipeRepository {
    @Throws(IOException::class)
    suspend fun getRecipeTypes(): List<Category>

    @Throws(IOException::class)
    suspend fun getRecipes(): List<Recipe>
}


class RecipeRepositoryImpl(private val assets:AssetManager) : IRecipeRepository {

    @Throws(IOException::class)
    override suspend fun getRecipeTypes(): List<Category> {
        val xml = read("recipe_types.xml")
        val mapper = XmlMapper()
        val type = object : TypeReference<List<Category>>() {}
        return mapper.readValue(xml, type)
    }

    @Throws(IOException::class)
    override suspend fun getRecipes(): List<Recipe> {
        val xml = read("recipes.xml")
        val mapper = XmlMapper()
        val type = object : TypeReference<List<Recipe>>() {}
        return mapper.readValue(xml, type)
    }

    private fun read(name: String): String {
        return try {
            assets.open(name).bufferedReader().use(BufferedReader::readText)
        } catch (ex: IOException) {
            ex.printStackTrace()
            ""
        }
    }
}