package net.hri.recipe.module

import net.hri.recipe.adapter.FlexibleAdapter
import net.hri.recipe.database.AppDatabase
import net.hri.recipe.dialog.CategoriesViewModel
import net.hri.recipe.ui.detail.DetailViewModel
import net.hri.recipe.ui.home.IRecipeRepository
import net.hri.recipe.ui.home.MainViewModel
import net.hri.recipe.ui.home.RecipeRepositoryImpl
import net.hri.recipe.util.prefs.SharePrefUtil
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module


val mAppModule = module {
    single { SharePrefUtil.with(androidContext()).ok() }
    single<IRecipeRepository> { RecipeRepositoryImpl(androidContext().assets) }
    single { AppDatabase.getInstance(androidContext()) }


    factory { FlexibleAdapter() }
}

val mViewModelModule = module {
    viewModel { MainViewModel(get(), get(), get()) }
    viewModel { DetailViewModel(get(), get()) }
    viewModel { CategoriesViewModel(get(),get()) }
}