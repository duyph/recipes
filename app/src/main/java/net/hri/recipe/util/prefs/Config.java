package net.hri.recipe.util.prefs;

import android.content.Context;

public class Config {
    private String mPreferenceName;
    private int mPreferenceMode;

    public Config(String preferenceName) {
        this(preferenceName, Context.MODE_PRIVATE);
    }

    public Config(String prefName, int mode) {
        mPreferenceName = prefName;
        this.mPreferenceMode = mode;
    }

    public int getPreferenceMode() {
        return mPreferenceMode;
    }

    public String getPreferenceName() {
        return mPreferenceName;
    }
}
