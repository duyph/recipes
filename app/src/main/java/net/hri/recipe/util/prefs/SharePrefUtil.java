package net.hri.recipe.util.prefs;

import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;

import java.lang.ref.WeakReference;

public class SharePrefUtil {
    private Config config;
    private WeakReference<Context> mWeakReference;

    private SharePrefUtil(Context context, Config config) {
        this.config = config;
        mWeakReference = new WeakReference<>(context);
    }

    /**
     * Save int value into share preferences
     *
     * @param name  the key
     * @param value the value map to the key
     */
    public void saveIntPref(String name, int value) {
        Context context = mWeakReference.get();
        if (context == null) return;
        SharedPreferences sharedPreferences = context.getSharedPreferences(config.getPreferenceName(),
                config.getPreferenceMode());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(name, value);
        editor.apply();
    }

    /**
     * Read and delete value from temp share preferences
     *
     * @param name the key
     * @param def  the bg_default value when the key is invalid
     * @return the value map to the key
     */
    public int readIntTempPref(String name, int def) {
        Context context = mWeakReference.get();
        if (context == null) return def;
        SharedPreferences sharedPreferences = context.getSharedPreferences(config.getPreferenceName(),
                config.getPreferenceMode());
        int val = sharedPreferences.getInt(name, def);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(name);
        editor.apply();
        return val;
    }

    /**
     * Save String value into share preferences
     *
     * @param name  the key
     * @param value the value map to the key
     */
    public void saveStringPref(String name, String value) {
        Context context = mWeakReference.get();
        if (context == null) return;
        SharedPreferences sharedPreferences = context.getSharedPreferences(config.getPreferenceName(),
                config.getPreferenceMode());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(name, value);
        editor.apply();
    }

    /**
     * Read and delete value from temp share preferences
     *
     * @param name the key
     * @param def  the bg_default value when the key is invalid
     * @return the value map to the key
     */
    public String readStringTempPref(String name, String def) {
        Context context = mWeakReference.get();
        if (context == null) return def;
        SharedPreferences sharedPreferences = context.getSharedPreferences(config.getPreferenceName(),
                config.getPreferenceMode());
        String val = sharedPreferences.getString(name, def);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(name);
        editor.apply();
        return val;
    }


    /**
     * Save boolean into share preferences
     *
     * @param name  the key
     * @param value the value map to the key
     */
    public void saveBooleanPref(String name, boolean value) {
        Context context = mWeakReference.get();
        if (context == null) return;
        SharedPreferences sharedPreferences = context.getSharedPreferences(config.getPreferenceName(),
                config.getPreferenceMode());
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(name, value);
        editor.apply();
    }

    /**
     * Save boolean into Secure preferences of android.
     * This method just work when app have role is system app
     *
     * @param name  the key
     * @param value the value map to the key
     */
    public void saveSecureBooleanPref(String name, boolean value) {
        Context context = mWeakReference.get();
        if (context == null) return;
        ContentResolver resolver = context.getContentResolver();
        Settings.Secure.putInt(resolver, name, value ? 1 : 0);
    }

    /**
     * Save Int into Secure preferences of android.
     * This method just work when app have role is system app
     *
     * @param name  the key
     * @param value the value map to the key
     */
    public boolean saveSecureIntPref(String name, int value) {
        Context context = mWeakReference.get();
        if (context == null) return false;
        ContentResolver resolver = context.getContentResolver();
        return Settings.Secure.putInt(resolver, name, value);
    }

    /**
     * Save Long into Secure preferences of android.
     * This method just work when app have role is system app
     *
     * @param name  the key
     * @param value the value map to the key
     */
    public boolean saveSecureLongPref(String name, long value) {
        Context context = mWeakReference.get();
        if (context == null) return false;
        ContentResolver resolver = context.getContentResolver();
        return Settings.Secure.putLong(resolver, name, value);
    }

    /**
     * Save String into Secure preferences of android.
     * This method just work when app have role is system app
     *
     * @param name  the key
     * @param value the value map to the key
     */
    public void saveSecureStringPref(String name, String value) {
        Context context = mWeakReference.get();
        if (context == null) return;
        ContentResolver resolver = context.getContentResolver();
        Settings.Secure.putString(resolver, name, value);
    }

    /**
     * Read value from temp share pref
     *
     * @param name the key
     * @param def  the bg_default value when the key is invalid
     * @return the value map to the key
     */
    public String readStringPref(String name, String def) {
        Context context = mWeakReference.get();
        if (context == null) return def;
        SharedPreferences sharedPreferences = context.getSharedPreferences(config.getPreferenceName(),
                config.getPreferenceMode());
        return sharedPreferences.getString(name, def);
    }

    /**
     * Read value from temp share pref
     *
     * @param name the key
     * @param def  the bg_default value when the key is invalid
     * @return the value map to the key
     **/
    public boolean readBooleanPref(String name, boolean def) {
        Context context = mWeakReference.get();
        if (context == null) return def;
        SharedPreferences sharedPreferences = context.getSharedPreferences(config.getPreferenceName(),
                config.getPreferenceMode());
        return sharedPreferences.getBoolean(name, def);
    }

    /**
     * Read value from share pref
     *
     * @param name the key
     * @param def  the bg_default value when the key is invalid
     * @return the value map to the key
     **/
    public int readIntPref(String name, int def) {
        Context context = mWeakReference.get();
        if (context == null) return def;
        SharedPreferences sharedPreferences = context.getSharedPreferences(config.getPreferenceName(),
                config.getPreferenceMode());
        return sharedPreferences.getInt(name, def);
    }

    /**
     * Read value from temp share pref
     *
     * @param name the key
     * @param def  the bg_default value when the key is invalid
     * @return the value map to the key
     **/
    public boolean readBooleanTempPref(String name, boolean def) {
        Context context = mWeakReference.get();
        if (context == null) return def;
        SharedPreferences sharedPreferences = context.getSharedPreferences(config.getPreferenceName(),
                config.getPreferenceMode());
        boolean val = sharedPreferences.getBoolean(name, def);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.remove(name);
        editor.apply();
        return val;
    }

    /**
     * Read Boolean value from Secure preferences of android.
     *
     * @param name the key
     * @param def  the bg_default value when the key is invalid
     * @return the value map to the key
     **/
    public boolean readSecureBooleanPref(String name, boolean def) {
        Context context = mWeakReference.get();
        if (context == null) return def;
        ContentResolver resolver = context.getContentResolver();
        int val = Settings.Secure.getInt(resolver, name, def ? 1 : 0);
        return val > 0;
    }

    /**
     * Read Int value from Secure preferences of android.
     *
     * @param name the key
     * @param def  the bg_default value when the key is invalid
     * @return the value map to the key
     **/
    public int readSecureIntPref(String name, int def) {
        Context context = mWeakReference.get();
        if (context == null) return def;
        ContentResolver resolver = context.getContentResolver();
        return Settings.Secure.getInt(resolver, name, def);
    }

    /**
     * Read Long value from Secure preferences of android.
     *
     * @param name the key
     * @param def  the bg_default value when the key is invalid
     * @return the value map to the key
     **/
    public long readSecureLongPref(String name, long def) {
        Context context = mWeakReference.get();
        if (context == null) return def;
        ContentResolver resolver = context.getContentResolver();
        return Settings.Secure.getLong(resolver, name, def);
    }

    /**
     * Read String value from Secure preferences of android.
     *
     * @param name the key
     * @return the value map to the key
     **/
    public String readSecureStringPref(String name) {
        Context context = mWeakReference.get();
        if (context == null) return null;
        ContentResolver resolver = context.getContentResolver();
        return Settings.Secure.getString(resolver, name);
    }

    public static Builder with(Context context) {
        return new Builder(context);
    }

    public static class Builder {
        private Context context;
        private Config config;

        private Builder(Context context) {
            this.context = context;
        }

        public Builder setConfig(String prefName) {
            this.config = new Config(prefName);
            return this;
        }

        public Builder setConfig(Config config) {
            this.config = config;
            return this;
        }

        public SharePrefUtil ok() {
            if (config == null) {
                config = new Config("bg_default-pref", Context.MODE_PRIVATE);
            }
            return new SharePrefUtil(context, config);
        }
    }
}
