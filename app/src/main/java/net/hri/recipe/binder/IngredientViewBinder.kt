package net.hri.recipe.binder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.DrawableRes
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.binder_ingredient_view.view.*
import net.hri.recipe.R
import net.hri.recipe.adapter.base.ItemViewBinder
import net.hri.recipe.model.Event
import net.hri.recipe.model.Ingredient
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class IngredientViewBinder(
    private val callback: OnIngredientCallback?=null
) : ItemViewBinder<Ingredient, IngredientViewBinder.ViewHolder>() {
    override fun onCreateViewHolder(
        inflater: LayoutInflater,
        parent: ViewGroup
    ): ViewHolder = ViewHolder(
        inflater.inflate(
            R.layout.binder_ingredient_view,
            parent, false
        ),
        callback
    )

    override fun onViewAttachedToWindow(holder: ViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.onViewAttachedToWindow()
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.onViewDetachedFromWindow()
    }

    override fun onBindViewHolder(holder: ViewHolder, item: Ingredient) {
        holder.setData(item)
    }

    class ViewHolder(view: View, callback: OnIngredientCallback?) : RecyclerView.ViewHolder(view) {
        var mIngredient: Ingredient? = null

        init {
            itemView.iv_clear.setOnClickListener { callback?.onIngredientClicked(this) }
        }

        fun setData(ingredient: Ingredient) {
            mIngredient = ingredient
            itemView.tv_name.text = ingredient.name
            itemView.tv_unit.text = ingredient.getAmountAndUnit()
            setIcon(ingredient.icon)
        }

        private fun setIcon(@DrawableRes resId: Int) {
            if (resId == 0) return
            itemView.tv_name.setCompoundDrawablesWithIntrinsicBounds(resId, 0, 0, 0)
        }

        @Suppress("unused")
        @Subscribe(threadMode = ThreadMode.MAIN)
        fun handleEditableUI(event: Event<Boolean>) {
           // itemView.iv_clear.visibility = if (event.data) View.VISIBLE else View.GONE
        }

        fun onViewAttachedToWindow() {
            EventBus.getDefault().register(this)
        }

        fun onViewDetachedFromWindow() {
            EventBus.getDefault().unregister(this)
        }
    }

    interface OnIngredientCallback {
        fun onIngredientClicked(holder: ViewHolder)
    }

}