package net.hri.recipe.binder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.binder_category_view.view.*
import net.hri.recipe.R
import net.hri.recipe.adapter.base.ItemViewBinder
import net.hri.recipe.model.Category

class CategoryViewBinder(private val callback: OnCategoryCallback) :
    ItemViewBinder<Category, CategoryViewBinder.ViewHolder>() {

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder =
        ViewHolder(inflater.inflate(R.layout.binder_category_view, parent, false), callback)

    override fun onBindViewHolder(holder: ViewHolder, item: Category) {
        holder.setData(item)
    }

    override fun notifySelectedState(data: Category?, isSelected: Boolean) {
        super.notifySelectedState(data, isSelected)
        data?.isSelected = isSelected
    }

    class ViewHolder(view: View, private val callback: OnCategoryCallback) :
        RecyclerView.ViewHolder(view) {
        var mCategory: Category? = null

        init {
            itemView.setOnClickListener { callback.onCategoryClicked(this) }
        }

        fun setData(category: Category) {
            this.mCategory = category
            itemView.tv_name.text = category.name
            itemView.iv_check.visibility = if (category.isSelected) View.VISIBLE else View.INVISIBLE
        }
    }

    interface OnCategoryCallback {
        fun onCategoryClicked(holder: ViewHolder)
    }
}