package net.hri.recipe.binder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.binder_add_more_ingredients_view.view.*
import kotlinx.android.synthetic.main.binder_ingredients_view.view.*
import net.hri.recipe.R
import net.hri.recipe.adapter.FlexibleAdapter
import net.hri.recipe.adapter.base.ItemViewBinder
import net.hri.recipe.dialog.IngredientsDialogFragment
import net.hri.recipe.model.Recipe
import org.koin.core.KoinComponent
import org.koin.core.inject

class IngredientsViewBinder(private val manager: FragmentManager) :
    ItemViewBinder<Recipe, IngredientsViewBinder.ViewHolder>() {

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder =
        ViewHolder(inflater.inflate(R.layout.binder_ingredients_view, parent, false), manager)

    override fun onBindViewHolder(holder: ViewHolder, item: Recipe) {
        holder.setData(item)
    }

    class ViewHolder(view: View, manager: FragmentManager) :
        RecyclerView.ViewHolder(view), KoinComponent,
        IngredientViewBinder.OnIngredientCallback {
        private val mAdapter: FlexibleAdapter by inject()
        private var mRecipe: Recipe? = null

        init {
            mAdapter.register(IngredientViewBinder(this))
            itemView.recycler_view.isNestedScrollingEnabled = false
            itemView.recycler_view.layoutManager = LinearLayoutManager(itemView.context)
            itemView.recycler_view.adapter = mAdapter

            itemView.tv_more_ingredient.setOnClickListener {
                val dialog = IngredientsDialogFragment(mRecipe?.ingredients)
                dialog.showNow(manager, null)
            }
        }

        fun setData(recipe: Recipe) {
            mRecipe = recipe
            mAdapter.submit(recipe.ingredients)
        }

        override fun onIngredientClicked(holder: IngredientViewBinder.ViewHolder) {

        }
    }
}