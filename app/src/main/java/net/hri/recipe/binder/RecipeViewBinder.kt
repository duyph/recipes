package net.hri.recipe.binder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.binder_recipe_view.view.*
import net.hri.recipe.R
import net.hri.recipe.adapter.base.ItemViewBinder
import net.hri.recipe.model.Recipe

class RecipeViewBinder(private val listener: OnRecipeListener) :
    ItemViewBinder<Recipe, RecipeViewBinder.ViewHolder>() {

    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder {
        val view = inflater.inflate(R.layout.binder_recipe_view, parent, false)
        return ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: ViewHolder, item: Recipe) {
        holder.setData(item)
    }

    class ViewHolder(view: View, listener: OnRecipeListener) : RecyclerView.ViewHolder(view) {
        var mRecipe: Recipe? = null

        init {
            itemView.setOnClickListener {
                listener.onRecipeClicked(this, it)
            }
        }

        fun setData(recipe: Recipe) {
            this.mRecipe = recipe
            Picasso.get().load(recipe.thumbnail)
                .error(R.drawable.bg_default)
                .placeholder(R.drawable.bg_default)
                .into(itemView.iv_thumbnail)
            itemView.tv_name.text = recipe.name
            itemView.tv_desc.text = recipe.desc

        }
    }

    interface OnRecipeListener {
        fun onRecipeClicked(holder: ViewHolder, view: View)
    }
}