package net.hri.recipe.binder

import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.binder_recipe_steps_view.view.*
import net.hri.recipe.R
import net.hri.recipe.adapter.base.ItemViewBinder
import net.hri.recipe.hideKeyboard
import net.hri.recipe.model.Event
import net.hri.recipe.model.Recipe
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode

class RecipeStepViewBinder : ItemViewBinder<Recipe.Content, RecipeStepViewBinder.ViewHolder>() {
    override fun onCreateViewHolder(inflater: LayoutInflater, parent: ViewGroup): ViewHolder =
        ViewHolder(inflater.inflate(R.layout.binder_recipe_steps_view, parent, false))

    override fun onBindViewHolder(holder: ViewHolder, item: Recipe.Content) {
        holder.setData(item)
    }

    override fun onViewAttachedToWindow(holder: ViewHolder) {
        super.onViewAttachedToWindow(holder)
        holder.onViewAttachedToWindow()
    }

    override fun onViewDetachedFromWindow(holder: ViewHolder) {
        super.onViewDetachedFromWindow(holder)
        holder.onViewDetachedFromWindow()
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view), TextWatcher {

        init {
            itemView.edt_step.addTextChangedListener(this)
        }

        fun setData(content: Recipe.Content) {
            itemView.edt_step.setText(content.content)
        }

        fun onViewAttachedToWindow() {
            EventBus.getDefault().register(this)
        }

        fun onViewDetachedFromWindow() {
            EventBus.getDefault().unregister(this)
        }

        @Suppress("unused")
        @Subscribe(threadMode = ThreadMode.MAIN)
        fun handleEditableUI(event: Event<Boolean>) {
            handleEditableFeature(event.data)
            focusable(event.data)
            val bgEditableTransparent = if (event.data) R.drawable.bg_editable_view
            else android.R.color.transparent
            itemView.edt_step.setBackgroundResource(bgEditableTransparent)
        }

        private fun handleEditableFeature(isEditable: Boolean) {
            if (!isEditable) itemView.edt_step.hideKeyboard()
        }

        private fun focusable(isEditable: Boolean) {
            itemView.edt_step.isFocusable = isEditable
            itemView.edt_step.isFocusableInTouchMode = isEditable
        }

        override fun afterTextChanged(s: Editable?) = Unit

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) = Unit

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            EventBus.getDefault().post(s.toString())
        }
    }

}