package net.hri.recipe

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.MutableLiveData
import java.io.Serializable
import java.util.*


class NonNullMediatorLiveData<T> : MediatorLiveData<T>()

interface BundleBuilder {
    infix fun String.to(value: Int)
    infix fun String.to(value: Long)
    infix fun String.to(value: Boolean)
    infix fun String.to(value: Double)
    infix fun String.to(value: Float)
    infix fun String.to(value: Byte)
    infix fun String.to(value: Parcelable)
    infix fun String.to(value: Serializable)
}

private class BundleBuilderImpl(arguments: Bundle?) : BundleBuilder {
    val bundle = arguments ?: Bundle()
    override fun String.to(value: Int) = bundle.putInt(this, value)
    override fun String.to(value: Long) = bundle.putLong(this, value)
    override fun String.to(value: Boolean) = bundle.putBoolean(this, value)
    override fun String.to(value: Parcelable) = bundle.putParcelable(this, value)
    override fun String.to(value: Serializable) = bundle.putSerializable(this, value)
    override fun String.to(value: Double) = bundle.putDouble(this, value)
    override fun String.to(value: Float) = bundle.putFloat(this, value)
    override fun String.to(value: Byte) = bundle.putByte(this, value)
}

fun bundle(arguments: Bundle? = null, configure: BundleBuilder.() -> Unit): Bundle {
    return BundleBuilderImpl(arguments).apply(configure).bundle
}

fun <T> LiveData<T>.nonNull(): NonNullMediatorLiveData<T> {
    val mediator: NonNullMediatorLiveData<T> =
        NonNullMediatorLiveData()
    mediator.addSource(this) { it?.let { mediator.value = it } }
    return mediator
}

fun <T> mutableLiveDataOf() = MutableLiveData<T>()

fun String.lower(): String {
    return this.toLowerCase(Locale.getDefault())
}

fun View.hideKeyboard(delay: Long = 0) {
    hideKeyboard(delay) { }
}

fun View.hideKeyboard(delay: Long = 0, callback: () -> Unit) {
    val imm: InputMethodManager? = context.getSystemService(InputMethodManager::class.java)
    postDelayed({
        imm?.hideSoftInputFromWindow(this.windowToken, 0)
        if (imm?.isAcceptingText == false) callback()
    }, delay)
}

fun String.toast(context: Context?) {
    Toast.makeText(context, this, Toast.LENGTH_SHORT).show()
}