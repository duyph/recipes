package net.hri.recipe.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import net.hri.recipe.SingletonHolder
import net.hri.recipe.model.Category
import net.hri.recipe.model.Recipe

@Database(entities = [Recipe::class, Category::class], version = 2, exportSchema = false)
@TypeConverters(IngredientsTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract val recipes: RecipeDao
    abstract val categories: CategoryDao

    companion object : SingletonHolder<AppDatabase, Context>({
        Room.databaseBuilder(
            it.applicationContext,
            AppDatabase::class.java, "recipes.db"
        ).fallbackToDestructiveMigration()
            .build()
    })
}