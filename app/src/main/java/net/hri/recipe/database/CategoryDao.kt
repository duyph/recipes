package net.hri.recipe.database

import androidx.room.*
import net.hri.recipe.model.Category

@Dao
interface CategoryDao {

    @Query("SELECT * FROM category_entity WHERE id=:id LIMIT 1")
    fun getCategory(id: Int): Category?

    @Query("SELECT COUNT(*) FROM recipe_entity")
    fun getCount(): Int


    @Query("SELECT * FROM category_entity")
    fun getAll(): List<Category>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(vararg recipes: Category)

    @Delete()
    fun delete(recipe: Category)
}