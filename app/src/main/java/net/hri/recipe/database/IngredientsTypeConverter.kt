package net.hri.recipe.database

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import net.hri.recipe.model.Ingredient


open class IngredientsTypeConverter {

    @TypeConverter
    fun convertIngredientsToJson(items: List<Ingredient>): String {
        return Gson().toJson(items)
    }

    @TypeConverter
    fun convertJsonToIngredients(json: String): List<Ingredient> {
        val type = object : TypeToken<List<Ingredient>>() {}.type
        return Gson().fromJson(json, type)
    }
}