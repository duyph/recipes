package net.hri.recipe.database

import androidx.room.*
import net.hri.recipe.model.Recipe

@Dao
interface RecipeDao {
    @Query("SELECT * FROM recipe_entity WHERE id=:id LIMIT 1")
    fun getRecipe(id: Int): Recipe?

    @Query("SELECT * FROM recipe_entity WHERE type=:type")
    fun getRecipesByType(type: Int): List<Recipe>

    @Query("SELECT COUNT(*) FROM recipe_entity")
    fun getCount(): Int

    @Query("SELECT * FROM recipe_entity")
    fun getAllRecipes(): List<Recipe>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun save(vararg recipes: Recipe)

    @Delete()
    fun delete(recipe: Recipe)

}