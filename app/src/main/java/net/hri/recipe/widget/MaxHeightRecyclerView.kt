package net.hri.recipe.widget

import android.content.Context
import android.util.AttributeSet
import androidx.recyclerview.widget.RecyclerView
import net.hri.recipe.R

class MaxHeightRecyclerView : RecyclerView {
    constructor(context: Context) : super(context) {}
    constructor(
        context: Context,
        attrs: AttributeSet?
    ) : super(context, attrs) {
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyle: Int
    ) : super(context, attrs, defStyle) {
    }

    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        val maxHeight = context.resources.getDimensionPixelOffset(R.dimen._300sdp)
        val height = MeasureSpec.makeMeasureSpec(maxHeight, MeasureSpec.AT_MOST)
        super.onMeasure(widthSpec, height)
    }
}